# -*- coding: utf-8 -*-

'''
Bibliotecas se trata de uma aplicação que visa demonstrar a funcionalidade multi-organizacional do framework.
A aplicação possibilita o cadastro de uma rede de bibliotecas espalhadas em regiões geográficas diferentes e que possuem seus próprios acervos e usuários.
Cada biblioteca possui seu administrador, que é responsáveis por cadastrar os bibliotecário e atendentes. Os bibliotecários cadastram os títulos e o acervo da bilioteca.
Os empréstimos são realizados pessoalmente e registrados pelos atendentes. Contudo, os usuários podem acessar o sistema para realizarem reservas de livros, consultar o acervo disponível e visualizar seus históricos de empréstimos.
Cada tipo de usuário possui um dashboard diferente contendo informações relevantes para seu respectivo perfil.
'''