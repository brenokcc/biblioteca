# -*- coding: utf-8 -*-

import datetime
from enderecos.models import Endereco
from pessoas.models import PessoaFisicaAbstrata
from djangoplus.admin.models import Organization
from djangoplus.db import models
from django.core.exceptions import ValidationError
from djangoplus.decorators import meta, action, subset, role


class Categoria(models.Model):
    """
    Cadastro utilizado para a classificação dos acervos das bibliotecas.
    """
    descricao = models.CharField('Descrição', example='Economia')
    categoria_pai = models.ForeignKey('biblioteca.Categoria', verbose_name='Categoria Pai', tree=True)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        verbose_female = True
        menu = 'Categorias', 'fa-th'
        usecase = 'UC001'

    def __str__(self):
        return self.descricao


class Biblioteca(Organization):
    """
    Representa uma unidade física da rede de bibliotecas. Localiza-se e regiões geográficas diversas e possui seus próprios usuários e acervos.
    """
    nome = models.CharField('Nome', example='Biblioteca Central')
    endereco = models.OneToOneField(Endereco, verbose_name='Endereço', null=True, blank=True)

    class Meta:
        verbose_name = 'Biblioteca'
        verbose_name_plural = 'Bibliotecas'
        verbose_female = True
        menu = 'Bibliotecas', 'fa-building'
        can_list_by_organization = 'Administrador'
        list_shortcut = True
        icon = 'fa-building'
        usecase = 'UC002'

    fieldsets = (
        ('Dados Gerais', {'fields': ('nome', 'endereco')}),
        ('Administradores::Administradores', {'relations': ('administrador_set',)}),
        ('Funcionários::Bibliotecários', {'relations': ('bibliotecario_set',)}),
        ('Funcionários::Atendentes', {'relations': ('atendente_set',)}),
    )

    def __str__(self):
        return self.nome


@role('cpf')
class Administrador(PessoaFisicaAbstrata):
    """
    Papel responsável por cadastrar os funcionários (bibliotecários e atendentes) das bibliotecas.
    """
    biblioteca = models.ForeignKey(Biblioteca, verbose_name='Biblioteca', example='Biblioteca Central', composition=True)

    class Meta:
        verbose_name = 'Administrador'
        verbose_name_plural = 'Administradores'
        usecase = 'UC003'


@role('cpf')
class Bibliotecario(PessoaFisicaAbstrata):
    """
    Papel responsável por cadastrar o acervo das bibliotecas.
    """
    biblioteca = models.ForeignKey(Biblioteca, verbose_name='Biblioteca', example='Biblioteca Central', composition=True)

    class Meta:
        verbose_name = 'Bibliotecário'
        verbose_name_plural = 'Bibliotecários'
        can_admin = 'Administrador'
        usecase = 'UC004'


@role('cpf')
class Atendente(PessoaFisicaAbstrata):
    """
    Papel responsável por registrar os empréstimos realizados presencialmente pelo usuários.
    """
    biblioteca = models.ForeignKey(Biblioteca, verbose_name='Biblioteca', example='Biblioteca Central', composition=True)

    class Meta:
        verbose_name = 'Atendente'
        verbose_name_plural = 'Atendentes'
        can_admin = 'Administrador'
        usecase = 'UC007'


class UsuarioManager(models.DefaultManager):

    @meta('Usuários por Município', can_view=('Administrador', 'Atendente'), dashboard='right')
    def total_usuarios_por_cidade(self):
        return self.all(self._user).count('endereco__municipio')


@role('cpf')
class Usuario(PessoaFisicaAbstrata):
    """
    Papel interessado em realizar os empréstimos de itens do acervo da biblioteca onde possui cadastro.
    Os empréstimos são realizados presencialmente na biblioteca e são registrados pelo antendentes. Os usuários podem acessar o sistema para realizar reservas e consultas.
    """
    biblioteca = models.ForeignKey(Biblioteca, verbose_name='Biblioteca', example='Biblioteca Central')

    fieldsets = PessoaFisicaAbstrata.fieldsets + (
        ('Empréstimos', {'relations': ('emprestimo_set',)}),
        ('Avaliações', {'relations': ('avaliacao_set',)}),
    )

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'
        can_admin_by_organization = 'Atendente'
        can_list_by_organization = 'Administrador'
        icon = 'fa-users'
        list_shortcut = 'Atendente'
        usecase = 'UC008'


class TituloManager(models.DefaultManager):

    @subset('Disponíveis para Empréstimo')
    def ativos(self):
        """Títulos que ainda não foram inativados e que, portanto, podem ser emprestados."""
        ids = Exemplar.objects.filter(ativo=True).values_list('titulo', flat=True).distinct()
        return self.filter(id__in=ids)

    @subset('Melhores Avaliados')
    def melhores_avaliados(self):
        """Títulos com notas maiores ou iguais que 7"""
        ids = Avaliacao.objects.exclude(nota__gt=7).values_list('titulo', flat=True).distinct()
        return self.filter(id__in=ids)

    @meta('Títulos por Categoria', can_view=('Administrador', 'Atendente', 'Bibliotecário'), dashboard='right', formatter='donut_chart')
    def total_por_categoria(self):
        return self.all(self._user).count('categoria')


class Titulo(models.Model):
    """
    Descritor de um cojunto de exemplares do mesmo tipo.
    """
    foto = models.ImageField(upload_to='fotos', verbose_name='Foto', sizes=((150, 200),), blank=True, null=True)
    nome = models.CharField('Nome', search=True, example='Economia & Cia')
    categoria = models.ForeignKey(Categoria, verbose_name='Categoria', filter=True, example='Economia')
    biblioteca = models.ForeignKey(Biblioteca, verbose_name='Biblioteca', example='Biblioteca Central')
    descricao = models.TextField('Descrição', search=True, example='Livro de economia')

    fieldsets = (
        ('Dados Gerais', {'fields': (('biblioteca', 'foto'), ('nome', 'categoria'), 'descricao')}),
        ('Exemplares::', {'fields': ('get_quantidade_exemplares',), 'relations': ('exemplar_set',)}),
        ('Avaliações::', {'relations': ('avaliacao_set',)}),
    )

    class Meta:
        verbose_name = 'Título'
        verbose_name_plural = 'Acervo'
        can_admin_by_organization = 'Bibliotecário'
        can_list_by_organization = 'Atendente', 'Usuário', 'Administrador'
        icon = 'fa-book'
        list_shortcut = 'Bibliotecário', 'Atendente', 'Usuário'
        list_template = 'image_rows.html'
        usecase = 'UC005'
        class_diagram = 'categoria', 'exemplar', 'biblioteca'
        list_per_page = 10

    def __str__(self):
        return self.nome

    def pode_ser_reservado(self):
        return self.exemplar_set.all().disponiveis().exists()

    @action('Reservar', can_execute='Usuário', inline=True, category=None)
    def reservar(self):
        """Reserva um exemplar do título para que o usuário possa pegá-lo fisicamente na biblioteca."""
        disponiveis = self.exemplar_set.all().disponiveis()
        if disponiveis.exists():
            disponiveis[0:1].update(reservado_para=self._user)
        else:
            raise ValidationError('Não existem mais exemplares dispíveis para esse título')

    @action('Avaliar', can_execute='Usuário', condition='pode_ser_avaliado', usecase='UC010', input='biblioteca.Avaliacao', inline=True, category=None)
    def avaliar(self, nota):
        """Registra a nota do usuário acerca do título."""
        usuario = Usuario.objects.get(cpf=self._user.username)
        Avaliacao.objects.create(titulo=self, nota=nota, usuario=usuario)

    def pode_ser_avaliado(self):
        """O título só pode ser avaliado pelo usuário uma única vez"""
        qs_usuario = Usuario.objects.filter(cpf=self._user.username)
        if qs_usuario.exists():
            return not Avaliacao.objects.filter(titulo=self, usuario=qs_usuario[0]).exists()
        return False

    @meta('Quantidade de Exemplares')
    def get_quantidade_exemplares(self):
        return self.exemplar_set.count()


class AvaliacaoManager(models.DefaultManager):

    @meta('Minhas Avaliações', can_view='Usuário', dashboard='right')
    def minhas_avaliacoes(self):
        """Avaliações realizadas pelo usuário autenticado"""
        return self.all(self._user)


class Avaliacao(models.Model):
    """
    Encapsula a nota de um determinado usuário acerca de um título específico
    """
    usuario = models.ForeignKey(Usuario, verbose_name='Usuário')
    titulo = models.ForeignKey(Titulo, verbose_name='Título')
    nota = models.IntegerField(choices=[(x, x) for x in range(1, 11)], example=8)

    class Meta:
        verbose_name = 'Avaliação'
        verbose_name_plural = 'Avaliações'
        verbose_female = True
        list_template = 'avaliacoes.html'
        can_list_by_role = 'Usuário'

    def __str__(self):
        return 'Avaliação do título {} por {}'.format(self.titulo, self.usuario)

    def get_estrelas(self):
        l = []
        for i in range(1, 6):
            if self.nota/2 >= i:
                l.append('star')
            else:
                if self.nota/2 == i-1 and self.nota % 2:
                    l.append('star-half-o')
                else:
                    l.append('star-o')
        return l


class ExemplarManager(models.DefaultManager):

    @subset('Disponíveis')
    def disponiveis(self):
        """Exemplares disponíveis para empréstimo"""
        return self.exclude(id__in=self.indisponiveis())

    @subset('Indisponíveis')
    def indisponiveis(self):
        """Exemplares emprestados no momento"""
        return self.filter(emprestimo__isnull=False, emprestimo__data_devolucao__isnull=True)

    @meta('Aquisição de Exemplares por Mês/Categoria', can_view='Bibliotecário', dashboard='center', formatter='bar_chart')
    def total_aquisicao_por_mes(self):
        """Total de aquisição de exemplares por mês e categoria"""
        return self.all(self._user).count('data_aquisicao', 'titulo__categoria')


class Exemplar(models.Model):
    """
    Item do acervo cujas informações estão descritas no título a ele associado.
    """
    codigo = models.CharField(verbose_name='Código', search=True, example='001')
    titulo = models.ForeignKey(Titulo, verbose_name='Título', example='Economia & Cia', composition=True)
    data_aquisicao = models.DateField('Data da Aquisição', example='01/01/2017')
    reservado_para = models.ForeignKey(Usuario, verbose_name='Reservado Para', exclude=True)
    ativo = models.BooleanField(verbose_name='Ativo', default=True)

    class Meta:
        verbose_name = 'Exemplar'
        verbose_name_plural = 'Exemplares'
        list_lookups = 'titulo__biblioteca'
        can_admin_by_organization = 'Bibliotecário'
        select_display = 'titulo__foto', 'codigo', 'titulo__nome'
        usecase = 'UC006'

    @action('Inativar')
    def inativar(self):
        """Inativa um item do acervo, impedido que ele possa ser utilizado em novos empréstimos."""
        self.ativo = True
        self.save()

    def __str__(self):
        return '{} - {}'.format(self.codigo, self.titulo)


class EmprestimoManager(models.DefaultManager):

    @meta('Total de Empréstimos Realizados', can_view='Usuário', dashboard='right')
    def quantidade_emprestimos_realizados(self):
        return self.meus_emprestimos().count()

    @meta('Meus Empréstimos', 'Usuário', dashboard='center')
    def meus_emprestimos(self):
        """Empréstimos realizados pelo usuário autenticado."""
        return self.all(self._user)

    @meta('Empréstimos por Mês', can_view=('Administrador', 'Atendente', 'Usuário'), dashboard='center', formatter='line_chart')
    def total_emprestimos_por_mes(self):
        return self.all(self._user).count('data_emprestimo')

    @subset('Não-Devolvidos', can_view='Atendente', can_notify=True)
    def nao_devolvidos(self):
        return self.all(self._user).filter(data_devolucao__isnull=True)


class Emprestimo(models.Model):
    """
    Registra as datas dos empréstimos realizados pelos usuários.
    """
    exemplar = models.ForeignKey(Exemplar, verbose_name='Exemplar', search=('codigo', 'titulo__descricao'), example='001')
    usuario = models.ForeignKey(Usuario, verbose_name='Usuário', search=('cpf', 'nome'), example='Juca da Silva')

    data_emprestimo = models.DateField('Data do Empréstimo', default=datetime.date.today, filter=True, example='01/01/2017')
    data_devolucao = models.DateField('Data da Devolução', exclude=True, example='10/01/2017')

    class Meta:
        verbose_name = 'Empréstimo'
        verbose_name_plural = 'Empréstimos'
        list_lookups = 'exemplar__titulo__biblioteca'
        can_list_by_role = 'Usuário'
        can_add_by_organization = 'Atendente'
        can_list_by_organization = 'Atendente', 'Administrador'
        add_label = 'Registrar Empréstimo'
        icon = 'fa-exchange'
        list_shortcut = 'Atendente'
        add_shortcut = 'Atendente'
        usecase = 'UC009'

    fieldsets = (
        ('Dados Gerais', {'relations': ('exemplar', 'usuario'),'fields': ('data_emprestimo', 'data_devolucao')}),
    )

    def choices(self):
        return dict(exemplar=Exemplar.objects.all().disponiveis())

    @action('Registrar Devolução', can_execute='Atendente', usecase='UC011', inline=True)
    def registrar_devolucao(self, data_devolucao):
        """Registra a data em que o livro foi devolvido pelo usuário na biblioteca"""
        self.data_devolucao = data_devolucao
        self.save()

    def __str__(self):
        return 'Empréstimo {}'.format(self.pk)

