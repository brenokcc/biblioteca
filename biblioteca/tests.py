# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase


class AppTestCase(TestCase):

    def test(self):
        User.objects.create_superuser(settings.DEFAULT_SUPERUSER, None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('Cadastrando Categoria')
    def cadastrar_categoria(self):
        self.click_menu('Categorias')
        self.click_button('Cadastrar')
        self.enter('Descrição', 'Economia')
        self.click_button('Salvar')

    @testcase('Cadastrando Biblioteca')
    def cadastrar_biblioteca(self):
        self.click_link('Bibliotecas')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Biblioteca Central')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Parnamirim')
        self.click_button('Salvar')

    @testcase('Adicionando Administrador em Biblioteca')
    def adicionar_administrador_em_biblioteca(self):
        self.click_link('Bibliotecas')
        self.click_icon('Visualizar')
        self.click_tab('Administradores')
        self.click_button('Adicionar Administrador')
        self.look_at_popup_window()
        self.enter('Nome', 'Juca da Silva')
        self.enter('CPF', '000.000.000-00')
        self.choose('Sexo', 'Masculino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Parnamirim')
        self.click_button('Salvar')

    @testcase('Adicionando Bibliotecário em Biblioteca')
    def adicionar_bibliotecario_em_biblioteca(self):
        self.click_link('Bibliotecas')
        self.click_icon('Visualizar')
        self.click_tab('Funcionários')
        self.look_at_panel('Bibliotecários')
        self.click_button('Adicionar Bibliotecário')
        self.look_at_popup_window()
        self.enter('Nome', 'Juca da Silva')
        self.enter('CPF', '000.000.000-00')
        self.choose('Sexo', 'Masculino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Parnamirim')
        self.click_button('Salvar')

    @testcase('Cadastrando Título')
    def cadastrar_titulo(self):
        self.click_link('Acervo')
        self.click_button('Cadastrar')
        self.enter('Foto', '')
        self.enter('Nome', 'Economia & Cia')
        self.choose('Categoria', 'Economia')
        self.enter('Descrição', 'Livro de economia')
        self.click_button('Salvar')

    @testcase('Adicionando Exemplar em Título')
    def adicionar_exemplar_em_titulo(self):
        self.click_link('Acervo')
        self.click_icon('Visualizar')
        self.click_tab('Exemplares')
        self.click_button('Adicionar Exemplar')
        self.look_at_popup_window()
        self.enter('Código', '001')
        self.enter('Data da Aquisição', '01/01/2017')
        self.click_button('Salvar')

    @testcase('Adicionando Atendente em Biblioteca')
    def adicionar_atendente_em_biblioteca(self):
        self.click_link('Bibliotecas')
        self.click_icon('Visualizar')
        self.click_tab('Funcionários')
        self.look_at_panel('Atendentes')
        self.click_button('Adicionar Atendente')
        self.look_at_popup_window()
        self.enter('Nome', 'Juca da Silva')
        self.enter('CPF', '000.000.000-00')
        self.choose('Sexo', 'Masculino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Parnamirim')
        self.click_button('Salvar')

    @testcase('Cadastrando Usuário')
    def cadastrar_usuario(self):
        self.click_link('Usuários')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Juca da Silva')
        self.enter('CPF', '000.000.000-00')
        self.choose('Sexo', 'Masculino')
        self.enter('Data de Nascimento', '27/08/1984')
        self.enter('CEP', '59.141-000')
        self.enter('Número', '123')
        self.enter('Complemento', 'Apartamento 100')
        self.enter('Logradouro', 'Centro')
        self.enter('Bairro', 'Cohabinal')
        self.choose('Município', 'Parnamirim')
        self.click_button('Salvar')

    @testcase('Registrando Empréstimo')
    def registrar_emprestimo(self):
        self.click_link('Empréstimos')
        self.click_button('Registrar Empréstimo')
        self.choose('Exemplar', '001')
        self.choose('Usuário', 'Juca da Silva')
        self.enter('Data do Empréstimo', '01/01/2017')
        self.click_button('Registrar Empréstimo')

    @testcase('Registrando Devolução')
    def registrar_devolucao_em_emprestimo(self):
        self.click_link('Empréstimos')
        self.click_icon('Visualizar')
        self.click_button('Registrar Devolução')
        self.look_at_popup_window()
        self.enter('Data da Devolução', '10/01/2017')
        self.click_button('Registrar Devolução')
